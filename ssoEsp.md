**SSO** Single Sign-on es una herramienta para autenticación de usarios que permite el acceso seguro a multiples aplicaciones y servicios utilizando un solo par de credenciales.

**SSO** está construido sobre el concepto de Identidad Federada, que consiste en compartir atributos sobre la identidad de un usuario a través de sistemas autonomos confiables o seguros. Cuando un usuario es verificado o reconocido, este tiene acceso inmediato al resto de los sistemas o servicios que forman parte de este (son de confianza.)

Podemos pensar en el **SSO** como un intermediario encargado de verificar las credenciales de un usuario, confirmar que el usuario es quien dice ser a cambio de un token que le permitirá acceder a las aplicaciones utilizando este servicio.

Existen varios tipos de protocolos y estandars para implementar y trabajar con **SSO**:

- **SAML**
- **OAuth / OIDC**

## SAML

Es un estándar abierto que define un esquema **XML** para el intercambio de datos de autenticación y autorización entre un Proveedor de Identidad (IDP) y un Proveedor de Servicios (SP).

En este caso el **IDP** autentica la identidad del usuario y envia la información mediante un **XML** al **SP** quien autoriza al usuario para acceder a los servicios.

Dado que el **IDP** es quien almacena y administra las credenciales del usuario, **SAML** permite a los **SPs** proveer sus servicios sin tener que implementar su propio sistema de autenticación. Los **SPs** se apoyan de **SAML** para aumentar la seguridad gracias a que no deben de manejar las credenciales de sus usuarios.

### Cómo funciona:

El protocolo consiste de 3 partes:

- **El usuario** - es quien requiere autenticarse para acceder a una aplicación / servicio. Todos los usuarios tienen Metadata también llamada información de indentidad, incluyendo nombre, apellido, correo electrónico, etc.

- **El IDP** es la plataforma que actua como la fuente de la información sobre la identidad y es quien se encarga de tomar las desiciones de autenticación. Un IDP puede pensarse como una base de datos que contiene todas las identidades de los usuarios.
  El IDP autentica al usuario y envía mediante un XML la información sobre la identidad del usuario al SP.

- **El SP** por otro lado es una aplicación / servicio a la que el usuario quiere acceder y que tiene implementado un sistema de SSO. Para autorizar el acceso a los recursos dentro del servicio, el SP toma una respuesta de autenticación del IDP y utiliza la información para crear y utilizar sesiones.

Una confirmación SAML es un XML que contiene información sobre autenticación y autorización sobre la identidad del usuario. Las confirmaciones SAML pueden ser consideradas como declaraciones o afirmaciones que el IDP está haciendo sobre un usuario.

Existen 3 tipos de confirmaciones:

- **De autenticación** - estas ayudan a confirmar la identidad del usuario.
- **De atributo** - estas proveen información sobre el usuario como nombre, apellidos, etc. Estos atributos deben de ser los mismos dentro del IDP y del SP.
- **De autorización** - son generalmente utilizadas cuando un usuario solicita un recurso en particular, estas comunican si el intento de autorización del usuario fue exitoso o no.
